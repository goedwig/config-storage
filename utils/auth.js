const Token = require('../models').tokens;


module.exports.isAuthorized = function (request, response, next) {
    if (!request.headers.authorization) {
        response.status(400).end();
        request.connection.destroy();
    }
    Token.findOne({ where: { token: request.headers.authorization.slice(7) } }).then(token => {
        if (!token) {
            response.status(401).end();
            request.connection.destroy();
        }
        request.username = token.username;
    }).then(result => {
        next();
    });
}