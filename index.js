const express = require('express');

const app = express();
app.use(express.json());

const db = require('./models');
db.sequelize.sync();
// drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
  // console.log("Drop and re-sync db.");
// });

const auth = require('./utils/auth');
const user = require('./routes/user');
const config = require('./routes/config');
app.use('/configs', auth.isAuthorized, config);
app.use('/', user);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});