const express = require('express');
const router = express.Router();

const Config = require('../models').configs;


router.get('/', function (request, response) {
    Config.aggregate('project', 'DISTINCT', { plain: false }).then(result => {
        let projects = []
        result.forEach(function (element) { projects.push(element.DISTINCT); });
        response.status(200).json(projects);
    })
});

router.get('/:project', function (request, response) {
    Config.findAll({ where: { project: request.params.project } }).then(result => {
        result.forEach(function (element) {
            if (element.config)
                // TODO decrypt
                element.config = JSON.parse(String.fromCharCode.apply(String, element.config));
        });

        response.status(200).json(result);
    });
});

router.post('/:project', function (request, response) {
    Config.create({
        project: request.params.project,
        config: Buffer.from(JSON.stringify(request.body)), // TODO ecrypt
        username: request.username
    }).then(result => {
        response.status(200)
    });
});

router.post('/:project/:id', function (request, response) {
    Config.findOne({ where: { project: request.params.project, id: request.params.id } }).then(result => {
        result.config = JSON.parse(String.fromCharCode.apply(String, result.config)); // TODO decrypt
        // apply(); if admin
        response.status(200);
    });
});


module.exports = router;
