const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const nanoid = require('nanoid/generate')

const User = require('../models').users;
const Token = require('../models').tokens;


router.post('/register', function (request, response) {
    bcrypt.hash(request.body.password, 10, function (error, hash) {
        User.create({
            username: request.body.username,
            password: hash
        }).error(error => {
            response.status(409).json('Пользователь уже существует.');
        });
    });
    response.status(200);
});

router.post('/login', function (request, response) {
    User.findOne({ where: { username: request.body.username } }).then(user => {
        bcrypt.compare(request.body.password, user.password.toString('utf8'), function (error, result) {
            if (result) {
                token = nanoid('1234567890abcdefghijklmnopqrstuvwxyz', 64);
                Token.upsert({
                    username: request.body.username,
                    token: token,
                    expired: new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
                });
                response.status(200).json(token);
            } else response.status(404).json('Имя пользователя или пароль введены неверно.');
        });
    }).error(error => {
        response.status(404).json('Имя пользователя или пароль введены неверно.');
    });
});

router.post('/logout', function (request, response) {
    Token.destroy({
        where: {
            token: request.headers.authorization.slice(7)
        }
    });
    response.redirect('/login');
});

module.exports = router;
