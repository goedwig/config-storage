/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    username: {
      type: DataTypes.STRING(32),
      allowNull: false,
      primaryKey: true
    },
    password: {
      type: "VARBINARY(64)",
      allowNull: false
    },
    is_admin: {
      type: DataTypes.INTEGER(1),
      defaultValue: 0,
      allowNull: false
    }
  }, {
    tableName: 'users'
  });
};
