/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('configs', {
    project: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    config: {
      type: "VARBINARY(65000)",
      allowNull: true
    },
    username: {
      type: DataTypes.STRING(32),
      allowNull: true,
      references: {
        model: 'users',
        key: 'username'
      }
    }
  }, {
    tableName: 'configs'
  });
};
